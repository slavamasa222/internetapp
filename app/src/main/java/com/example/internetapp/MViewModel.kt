package com.example.internetapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class MViewModel: ViewModel() {

    private val _response = MutableLiveData<String>()
    val response: LiveData<String>
        get() = _response

    init {
        getUserProperties()
    }

    private fun getUserProperties() {

        viewModelScope.launch {

            try {
                val properties = UserApi.retrofitService.getProperties()
                _response.value = properties.response[0].toString()
            }
            catch (e: Exception) {
                _response.value = "Failure: ${e.message}"
            }
        }
    }
}