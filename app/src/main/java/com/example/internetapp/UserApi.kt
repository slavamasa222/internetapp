package com.example.internetapp

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET

private const val BASE_URL =
    "https://api.vk.com/"
private const val SERVICE_TOKEN =
    "7e5e9b417e5e9b417e5e9b41397e29fe6a77e5e7e5e9b411e2990c6feb6e643fc9a8861"
private const val USER_ID =
    "219521078"
private const val FIELDS =
    "sex"
private const val VERSION =
    "5.130"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface UserApiService {
    @GET("method/users.get?user_id=$USER_ID" +
            //"&fields=$FIELDS" +
            "&v=$VERSION" +
            "&access_token=$SERVICE_TOKEN" )
    suspend fun getProperties(): UserProperty
}

object UserApi {
    val retrofitService : UserApiService by lazy {
        retrofit.create(UserApiService::class.java) }
}