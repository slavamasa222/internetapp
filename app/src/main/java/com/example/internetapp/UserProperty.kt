package com.example.internetapp

import com.squareup.moshi.Json

data class UserProperty(

    @Json(name = "response") val response: List<ResponseProperty>
)

data class ResponseProperty(

    @Json(name = "id") val id: Int,
    @Json(name = "first_name") val firstName: String,
    @Json(name = "last_name") val lastName: String,
    @Json(name = "is_closed") val isClosed: Boolean,
    @Json(name = "can_access_closed") val canAccessClosed: Boolean
)